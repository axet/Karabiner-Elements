#!/bin/bash

set -e

git fetch --all
git rebase a/master

rm -f *.dmg
make clean

./scripts/build-and-install.sh --no-rebase
